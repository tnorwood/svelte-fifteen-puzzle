import { writable, derived } from 'svelte/store';

let squaresDefault = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16];

function countInversions(squares) {
  return squares.reduce((x, n, i, a) => {
    return x + (i ? a.slice(i).reduce((y, m, j, b) => {
      return y + (m < n ? 1 : 0)
    }, 0) : 0)
  }, 0);
}

function swap(array, index) {
  let a = array.slice();
  [ a[a.indexOf(16)], a[index] ] = [ a[index], 16 ];
  return a;
}

// Durstenfield Shuffle
function shuffle(array) {
  let a = squaresDefault.slice();

  while(countInversions(a) % 2 === 0) {
    for (let i = a.length - 1; i > 0; i--) {
      let j = Math.floor(Math.random() * (i + 1));
      [ a[i], a[j] ] = [ a[j], a[i] ];
    }

    [ a[a.indexOf(16)], a[0] ] = [ a[0], 16 ];
  }

  return a;
}

function createSquares() {
  const { subscribe, set, update } = writable(squaresDefault);

  return {
		subscribe,
    swap: m => update(a => swap(a, m)),
    shuffle: () => update(a => shuffle(a)),
		reset: () => set(squaresDefault)
	};
}


export const squares = createSquares();

export const gameWon = derived(squares, $squares => countInversions($squares) === 0);
